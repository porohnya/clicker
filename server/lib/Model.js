/**
 * Core Model Class
 *
 * @name Model
 * @namespace
 * @param {Object} params
 * @param {String} modelName
 * @constructor
 */
var Model = function (params, modelName) {
    params = params || {};
    this.modelName = modelName;
    this._params = this._params || {};

    Object.keys(this._params).forEach(function (key) {
        this.__defineGetter__(key, function () {
            return this.getterFunction(key);
        });

        this.__defineSetter__(key, function (value) {
            return this.setterFunction(key, value);
        });
    }, this);

   Object.keys(params).forEach(function (key) {
       this.setterFunction(key, params[key]);
   }, this);
};

/**
 * @lends Model
 */
Model.prototype.extend({

    /**
     * Update params
     * @param {Object} params
     */
    updateParams: function (params) {
        Object.keys(params).forEach(function (key) {
            this.setterFunction(key, params[key]);
        }, this);
    },

    /**
     * Returns params for client
     *
     * @returns {Object}
     */
    toExport: function () {
        var result = {};
        result.extend(this._params);
        return result;
    },

    /**
     * @TODO
     * Load model from database
     * @param {Function} callback
     */
    load: function (callback) {
        callback = callback || function () {};
        callback();
    },


    /**
     *
     * Save model from database
     * @param {Function} callback
     */
    save: function (callback) {
        callback = callback || function () {};
        var data = this.toExport();
        application.mongo.save(this.modelName, data, function () {
            callback();
        });
    },


    /**
     * @TODO
     * Remove model from database
     * @param {Function} callback
     */
    remove: function (callback) {
        callback = callback || function () {};
        callback();
    },

    /**
     * Getter function from _param
     * @param {String} paramName
     * @returns {*}
     */
    getterFunction: function (paramName) {
        return this._params[paramName];
    },

    /**
     * Setter function
     *
     * @param {String} paramName
     * @param {*} paramValue
     */
    setterFunction: function (paramName, paramValue) {
        if (typeof paramValue !== 'function') {
            this._params[paramName] = paramValue;
        }
    }

});

module.exports = Model;