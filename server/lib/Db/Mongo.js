var Db = require('mongodb').Db,
    Server = require('mongodb').Server,
    mongodb = require('mongodb'),
    async = require('async');

module.exports = {

    _db: null,

    connect: function (params, callback) {
        var self = this;
        this.params = params;
        this._db = new Db(params.database, new Server(params.host, params.port, {auto_reconnect: true}),
            {
                safe: true,
                w:1,
                wtimeout:0
            });
        async.waterfall([
            function (next) {
                self._db.open(next);
            },
            function (db, next) {
                next();
            }
        ], callback);
    },

    getList: function (collectionName, criteria, sort, callback) {
        if (typeof sort === 'function') {
            callback = sort;
        }
        var collection = new mongodb.Collection(this._db, collectionName);
        var cursor = collection.find(criteria);
        if (typeof sort !== 'function') {
            cursor.sort(sort);
        }

        cursor.toArray(function (error, docs) {
            if (error) {
                callback(error);
            } else {
                callback(null, docs);
            }
        });
    },

    /**
     * Drops Collection
     * @param {String} collectionName
     * @param {Function} callback
     */
    dropCollection: function (collectionName, callback) {
        var self = this;
        async.waterfall([
            function (next) {
                self._db.collectionNames(collectionName, {namesOnly: true}, next);
            },
            function (collectionList, next) {
                if (!collectionList.length) {
                    console.warn('Collection "' + collectionName + '" not found while dropping');
                    return next();
                }
                var collection = new mongodb.Collection(self._db, collectionName);
                collection.drop(next);
            }
        ], callback);
    },

    getOne: function (collectionName, criteria, callback) {
        var collection = new mongodb.Collection(this._db, collectionName);
        collection.findOne(criteria, callback);
    },

    save: function (collectionName, entity, callback) {
        callback = callback || function () {};
        var self = this,
            collection = new mongodb.Collection(this._db, collectionName);

        async.waterfall([
            function (next) {
                if (entity._id) {
                    return next(null, entity._id);
                }
                self.getNewId(collectionName, next);
            },
            function (id, next) {
                entity._id = id;
                collection.save(entity, next);
            },
            function (obj, stats, next) {
                next();
            }
        ], callback);
    },

    insert: function (collectionName, entity, callback) {
        callback = callback || function () {};
        var self = this,
            collection = new mongodb.Collection(this._db, collectionName);

        async.waterfall([
            function (next) {
                if (entity._id) {
                    return next(null, entity._id);
                }
                self.getNewId(collectionName, next);
            },
            function (id, next) {
                entity._id = id;
                collection.insert(entity, next);
            },
            function (obj, next) {
                next();
            }
        ], callback);
    },

    remove: function (collectionName, criteria, callback) {
        callback = callback || function () {};
        var collection = new mongodb.Collection(this._db, collectionName);
        collection.remove(criteria, callback);
    },


    getNewId: function (collectionName, callback) {
        var self = this,
            id;
        async.waterfall([
            function (next) {
                self.getOne('seq', {_id: collectionName}, next);
            },
            function (doc, next) {
                if (!doc) {
                    id = 0;
                } else {
                    id = doc.value;
                }
                id += 1;

                self.save('seq', {_id: collectionName, value: id}, next);
            },
            function (next) {
                next(null, id);
            }
        ], callback);
    }
};