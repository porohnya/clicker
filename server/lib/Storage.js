/**
 * @name Storage
 * @namespace
 * @constructor
 */
var Storage = function () {

    this.users = {};
    this.dictionary = {};

    this._exportCache = null;
};
/**
 * @lends Storage
 */
Storage.prototype.extend({

    /**
     * Add user to storage
     * @param {UserModel} user
     */
    addUser: function (user) {
        if (!this.users[user._id]) {
            this.users[user._id] = user;
        }
    },

    /**
     * Returns user by id
     * @param {Number} id
     * @returns {UserModel}
     */
    getUser: function (id) {
        return this.users[id];
    },

    /**
     * Remove user from storage
     * @param {Number} id
     */
    removeUser: function (id) {
        delete this.users[id];
    },


    /**
     * Returns model by id
     * @param {String} modelName
     * @param {Number} id
     * @returns {Model}
     */
    getModel: function (modelName, id) {
        return this.dictionary[modelName][id];
    },

    /**
     * Adds model to the dictionary
     * @param {String} modelName
     * @param {Model} modelInstance
     */
    addModel: function (modelName, modelInstance) {
        if (!this.dictionary[modelName]) {
            this.dictionary[modelName] = {};
        }

        if (!this.dictionary[modelName][modelInstance._id]) {
            this.dictionary[modelName][modelInstance._id] = modelInstance;
        }
    },

    /**
     * Returns all models
     * @param {String} modelName
     * @returns {Object}
     */
    getModels: function (modelName) {
        return this.dictionary[modelName];
    },

    /**
     * Export models to client format
     * @returns {Object}
     */
    exportModels: function () {
        if (this._exportCache) {
            return this._exportCache;
        }
        var result = {};
        Object.keys(this.dictionary).forEach(function (modelName) {
            result[modelName] = {};
            Object.keys(this.dictionary[modelName]).forEach(function (id) {
                result[modelName][id] = this.dictionary[modelName][id].toExport();
            }, this);
        }, this);

        this._exportCache = result;
        return result;
    }
});


module.exports = Storage;