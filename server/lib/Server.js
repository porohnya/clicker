var io = require('socket.io');
var util = require('util');
var EventEmitter = require('events').EventEmitter;
var Server = function (config) {

    this.config = config;
    this.server = null;
};

util.inherits(Server, EventEmitter);

Server.prototype.extend({

    run: function () {
        var self = this;
        this.server = io.listen(this.config.port);
        this.server.sockets.on('connection', function (socket) {
            self.emit('connection', socket);

            socket.on('disconnect', function () {
                self.emit('disconnect', socket);
            });
        });


        /**
        setInterval(function () {
            console.log('--------------------------');
            console.log('Current connections: ')

            var socketIds = Object.keys(this.server.sockets.sockets);

            socketIds.forEach(function (id) {
                var socket = this.server.sockets.sockets[id];
                console.log('User: ', socket._userId, 'Socket: ', socket.id);
            }, this);


            console.log('Storage: ')
            Object.keys(application.storage.users).forEach(function (userId) {
                var user = application.storage.getUser(userId);
                console.log('User: ', user._id, 'Socket ', (user.socket) ? user.socket.id : 'n/a');
            });
        }.bind(this), 1000)*/
    }
});

module.exports = Server;