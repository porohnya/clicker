/*global application*/
var async = require('async');
var fs = require('fs');
/**
 * @name Installer
 * @namespace
 * @param {object} params
 * @constructor
 */
var Installer = function (params) {

    this.params = params || {}
};


/**
 * @lends Installer
 */
Installer.prototype.extend({

    /**
     * Run installation
     *
     * @param {function} callback
     */
    run: function (callback) {
        callback = callback || function() {};
        var self = this;
        async.waterfall([
            self.clearDb.bind(self),
            self.clearDbSequences.bind(self),
            self.uploadDb.bind(self)
        ],callback)
    },


    /**
     * Clear collections described in config.js
     * @param {function} callback
     */
    clearDb: function (callback) {
        async.forEach(this.params.dropCollections, function (collectionName, next) {
            console.log('Dropping collection "' + collectionName + '"');
            application.mongo.dropCollection(collectionName, next);
        }, callback);
    },

    /**
     * Clear collections seq
     *
     * @param callback
     */
    clearDbSequences: function (callback) {
        async.forEach(this.params.dropCollections, function (collectionName, next) {
            application.mongo.remove('_seq', {_id: collectionName}, next);
        }, callback);
    },

    /**
     * Uploads data to db from database.json file
     * @param {function} callback
     */
    uploadDb: function (callback) {
        console.log('Uploading DB');
        var data;
        var _uploadCollection = function (collectionName, callback) {
            var entities = data[collectionName];
            async.eachSeries(entities, function (entity, next) {
                application.mongo.insert(collectionName, entity, next);
            }, callback);

        };


        async.waterfall([
            function (next) {
                fs.readFile('./install/database.json', next);
            },
            function (content, next) {
                data = JSON.parse(content);
                async.forEach(Object.keys(data), _uploadCollection, next);
            }

        ],callback);

    }
});


module.exports = Installer;