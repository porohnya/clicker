var async = require('async');
/**
 * @name Helper
 * @namespace
 */
var Helper = {

    /**
     * Returns random integer
     * @param {Number} min
     * @param {Number} max
     * @returns {Number}
     */
    getRandomInt: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },


    /**
     * Returns user data by UUID
     * @param {String} uuid
     * @param {Function} callback
     */
    getUserByUUID: function (uuid, callback) {
        application.mongo.getOne('User', {uuid: uuid}, callback);
    },


    /**
     * Get roll by percentage chance
     *
     * @param {{chance:number, value:*}[]} elements
     * @param {Number} maxValue
     * @param {*} defaultValue
     * @returns {*}
     */
    getRandomValueByChance: function (elements, maxValue, defaultValue) {
        var roll = this.getRandomInt(0, maxValue);
        var result = 0;
        var min = 0;
        var max = 0;
        //console.log('------------------ ROLL: ', roll, '--------------------');
        elements.forEach(function (element) {
            element.min = min;
            element.max = element.min + element.chance;
            min = element.max;
            max = min;
            //console.log('min', element.min, 'max', element.max);
        });

        if (max < maxValue) {
            elements.push({
                isDummy: true,
                chance: maxValue - max,
                min: max,
                max: maxValue
            });
        }

        elements.some(function (element, index) {
            if (element.max >= roll && element.min < roll) {
                result = index;
            }
        });

        if (elements[result].isDummy) {
            return defaultValue;
        }

        return elements[result].value;

    },

    /**
     * Register user by uuid
     *
     * @param {String} uuid
     * @param {Object} data
     * @param {Function} callback
     */
    registerUserByUUID: function (uuid, data, callback) {
        var userData = data || {};
        userData.uuid = uuid;
        var self = this;
        async.waterfall([
            function (next) {
                application.mongo.save('User', userData, next);
            },
            function (next) {
                self.getUserByUUID(uuid, next);
            }
        ], callback);
    }
};

module.exports = Helper;