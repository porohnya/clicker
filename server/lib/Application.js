var async = require('async');
var Storage = require('./Storage');
var Server  = require('./Server');
var Mongo = require('./Db/Mongo');
var Helper = require('./Helper');
var UserModel = require('../models/User');
var Installer = require('./Installer');
var redis = require('redis');

/**
 * @name Application
 * @namespace
 * @param {object} config
 * @param {object} startParams
 * @constructor
 */
var Application = function (config, startParams) {
    this.config = config;

    this.server = new Server(this.config.server);

    this.storage = new Storage();

    this.mongo = Mongo;

    this.helper = Helper;
    
    this.startParams = startParams;
};


Application.prototype.extend({

    /**
     * initialize Application
     */
    initialize: function () {
        var self = this;
        async.waterfall([
            function (next) {
                self.mongo.connect(self.config.mongo, next);
            },
            function (next) {
                console.log('Database connected');
                self.server.run();
                self.server.on('connection', self.onConnect.bind(self));
                self.server.on('disconnect', self.onDisconnect.bind(self));
                self._initRedis(next);
            },
            function (next) {
                console.log('Redis connected');
                self.lock = require("redis-lock")(self.redis);
                self._loadModels(next);
            }

        ], function (error) {
            if (error) {
                console.error('Server starup error: ', error);
                process.exit(1);
            }
            
            if (self.startParams.hasOwnProperty('--install')) {
				self._installMode();
			}

        });
    },


    /**
     * Initialize Redis DB
     * @param {function} callback
     * @private
     */
    _initRedis: function (callback) {
        var options = this.config.redis.options || {};
        this.redis = redis.createClient(this.config.redis.port, this.config.redis.host, options);
        this.redis.on('ready', callback);
        this.redis.on('error', callback);
    },

    /**
     * Running install mode
     *
     * @private
     */
    _installMode: function () {
		console.log('----------- INSTALL MODE ----------------');
        var config = require('../install/config');
        var installer = new Installer(config);
        installer.run(function (error) {
            if (error) {
                console.log('Installation error: ', error);
                process.exit(1);
                return;
            }
            console.log('Installation complete. Please restart server');
            process.exit(0);
        })
	},

    /**
     * Load models to application storage
     *
     * @param {Function} callback
     * @private
     */
    _loadModels: function (callback) {

        async.eachSeries(this.config.models, function (modelConfig, next) {
            console.log('Loading model "' + modelConfig.name + '"');
            application.mongo.getList(modelConfig.name, {}, function (error, dataList) {
                if (error) {
                    return next(error);
                }
                var ModelClass = require('../models/' + modelConfig.name);

                dataList.forEach(function (data) {
                    var modelInstance = new ModelClass(data);
                    application.storage.addModel(modelConfig.name, modelInstance);

                });
                next(null);
            });
        }, callback)
    },


    /**
     * new socket connection handler
     *
     * @param {Socket} socket
     */
    onConnect: function (socket) {
        console.log('new connection established', socket.id);
        var user = new UserModel();
        user.socket = socket;
        user.bind();
        user.notify('whoareyou', {});
    },

    /**
     * Disconnection handler
     *
     * @param {Socket} socket
     */
    onDisconnect: function (socket) {
        var user = application.storage.getUser(socket._userId);
        if (user) {

            user.onDisconnect();
        }
    }

});


module.exports = Application;
