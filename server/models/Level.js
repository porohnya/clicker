var util = require('util');
var Model = require('../lib/Model');
var Helper = require('../lib/Helper');

/**
 * Level model
 *
 * @namespace
 * @name LevelModel
 * @inherits Model
 * @param {Object} params
 * @constructor
 */
var LevelModel = function (params) {
    this._params = {
        /**
         * ID
         * @type {Number}
         */
        _id: null,


        /**
         * Level
         * @type {Number}
         */
        level: 0
    };

    Model.call(this,params);
};

util.inherits(LevelModel, Model);

/**
 * @lends LevelModel
 */
LevelModel.prototype.extend({

    /**
     * Get current level experience
     * @returns {number}
     */
    get experience() {
        return Math.pow(this.level, 2) * 50 + 100;
    },


    /**
     * Get current level energy
     * @returns {number}
     */
    get energy() {
        return Math.floor(49 + Math.pow(this.level, 1.5688));
    },

    /**
     * Get current level money reward
     * return {number}
     */
    get moneyReward() {
        return Math.floor(Math.pow(this.level, 1.5) * 100);
    }

});

module.exports = LevelModel;