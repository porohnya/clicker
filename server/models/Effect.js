var util = require('util');
var Model = require('../lib/Model');

/**
 * Effect model
 *
 * @namespace
 * @name EffectModel
 * @inherits Model
 * @param {Object} params
 * @constructor
 */
var EffectModel = function (params) {

    this._params = {
        /**
         * Save progress steps
         */
        saveProgress: 0,

        /**
         * Fix level
         */
        fixLevel: 0
    };



    Model.call(this,params);
};

util.inherits(EffectModel, Model);

/**
 * @lends EffectModel
 */
EffectModel.prototype.extend({

});

module.exports = EffectModel;