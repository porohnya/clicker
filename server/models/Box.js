var util = require('util');
var Model = require('../lib/Model');
var Helper = require('../lib/Helper');

/**
 * Box model
 *
 * @namespace
 * @name BoxModel
 * @inherits Model
 * @param {Object} params
 * @constructor
 */
var BoxModel = function (params) {
    this._params = {
        /**
         * ID
         * @type {Number}
         */
        _id: null,

        /**
         * Name
         * @type {String}
         */
        name: null,

        /**
         * Level
         * @type {Number}
         */
        level: 0,

        /**
         * Chance to unbox lower box
         * @type {Number}
         */
        lowerBoxChance: 0,

        /**
         * Chance to unbox current box
         * @type {Number}
         */
        currentBoxChance: 0,

        /**
         * Chance to unbox higher box
         * @type {Number}
         */
        higherBoxChance: 0,


        /**
         * Box fixation
         * @type {Boolean}
         */
        fixation: false,

        /**
         * Drop ids
         * @type {Number[]}
         */
        dropIds: []
    };

    Model.call(this,params, 'Box');
};

util.inherits(BoxModel, Model);

/**
 * @lends BoxModel
 */
BoxModel.prototype.extend({

    /**
     * Returns drop model list
     *
     * @returns {DropModel[]}
     */
    get drop() {
        var result = [];
        this.dropIds.forEach(function (dropId) {
            result.push(application.storage.getModel('Drop', dropId));
        });

        return result;
    },

    /**
     * Min experience
     * @returns {number}
     */
    get minExperience() {
        return Math.ceil(Math.pow(this.level, 2) * 0.8 + 2);
    },

    /**
     * Max experience
     * @returns {number}
     */
    get maxExperience() {
        return Math.ceil(Math.pow(this.level, 2) * 2 + 8);
    },

    /**
     * Min money
     * @returns {number}
     */
    get minMoney() {
        return Math.ceil(Math.pow(this.level, 1.7) * 5);
    },

    /**
     * Max money
     * @returns {number}
     */
    get maxMoney() {
        return Math.ceil(Math.pow(this.level, 1.6) * 12);
    },

    /**
     * Rolls Drop
     *
     * @param {Number} attempt
     * @returns {null|{attempt: number, energy: number, money: number, experience: number, invite: number}}
     */
    rollDrop: function (attempt) {

        if (attempt === 2 && Helper.getRandomInt(0, 100) > 25) {
            return null;
        }

        if (attempt === 3 && Helper.getRandomInt(0, 100) > 8) {
            return null;
        }

        var drops = this.drop;
        var rollObj = [];
        if (!drops.length) {
            return null;
        }

        drops.forEach(function (drop) {
            rollObj.push({
                chance: drop.chance || 0,
                value: drop
            });
        }, this);

        var rolledDrop = Helper.getRandomValueByChance(rollObj, 100, null);

        var dropResult = {
            attempt: attempt,
            experience: 0,
            money: 0,
            invite: 0,
            energy: 0
        };

        if (rolledDrop === null) {
            return null;
        }


        if (rolledDrop.isMoney) {
            dropResult.money +=  Helper.getRandomInt(this.minMoney, this.maxMoney);
        }

        if (rolledDrop.isExperience) {
            dropResult.experience +=  Helper.getRandomInt(this.minExperience, this.maxExperience);
        }

        if (rolledDrop.energy) {
            dropResult.energy += rolledDrop.energy;
        }

        if (rolledDrop.invite) {
            dropResult.invite += rolledDrop.invite;
        }

        return dropResult;
    },

    /**
     * Unboxing current box
     *
     * @returns {{drops: [], newBoxId: Number}}
     */
    unBox: function () {

        var boxDelta = Helper.getRandomValueByChance([
           {chance: this.lowerBoxChance * 10, value: -1},
           {chance: this.currentBoxChance * 10, value: 0},
           {chance: this.higherBoxChance * 10, value: 1}
        ], 1000, this.boxId);

        var boxesIds =  Object.keys(application.storage.dictionary['Box']);

        var newBoxLevel = this.level + boxDelta;
        if (newBoxLevel === 0) {
            newBoxLevel = 1;
        }
        var newBoxId = null;
        boxesIds.some(function (boxId) {
            var boxModel = application.storage.getModel('Box',boxId)
            if (newBoxLevel === boxModel.level) {
                newBoxId = boxModel._id;
                return true;
            }
        }, this);

        if (!newBoxId) {
            newBoxId = this._id;
        }

        var drops = [];
        var firstAttemptDrop = this.rollDrop(1);
        var secondAttemptDrop = this.rollDrop(2);
        var thirdAttemptDrop = this.rollDrop(3);
        if (firstAttemptDrop) {
            drops.push(firstAttemptDrop);
        }

        if (firstAttemptDrop && secondAttemptDrop) {
            drops.push(secondAttemptDrop);
        }

        if (secondAttemptDrop && thirdAttemptDrop) {
            drops.push(thirdAttemptDrop);
        }

        return {
            drops: drops,
            newBoxId: newBoxId
        }
    }
});

module.exports = BoxModel;