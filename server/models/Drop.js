var util = require('util');
var Model = require('../lib/Model');

/**
 * Drop model
 *
 * @namespace
 * @name DropModel
 * @inherits Model
 * @param {Object} params
 * @constructor
 */
var DropModel = function (params) {
    this._params = {
        /**
         * ID
         * @type {Number}
         */
        _id: null,

        /**
         * Energy count
         * @type {Number}
         */
        energy: 0,

        /**
         * Invite
         * @type {Number}
         */
        invite: 0,

        /**
         * Is money flag
         * @type {Boolean}
         */
        isMoney: false,

        /**
         * Is experience flag
         * @type {Boolean}
         */
        isExperience: false,

        /**
         * Drop chance
         * @type {Number}
         */
        chance: 0

    };

    Model.call(this, params, 'Drop');
};

util.inherits(DropModel, Model);

/**
 * @lends BoxModel
 */
DropModel.prototype.extend({

});

module.exports = DropModel;