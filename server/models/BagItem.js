var util = require('util');
var Model = require('../lib/Model');


/**
 * BagItem model
 *
 * @namespace
 * @name BagItem
 * @inherits Model
 * @param {Object} params
 * @constructor
 */
var BugItemModel = function (params) {
    this._params = {
        /**
         * ID
         * @type {Number}
         */
        shopItemId: null,

        /**
         * Count
         * @type {Number}
         */
        count: 0
    };

    Model.call(this,params, 'BugItem');
};

util.inherits(BugItemModel, Model);

/**
 * @lends BugItemModel
 */

BugItemModel.prototype.extend({

    /**
     * Returns shopItem
     * @returns {ShopItemModel}
     */
    get shopItem() {
        return application.storage.getModel('ShopItem', this.shopItemId);
    }

});

module.exports = BugItemModel;