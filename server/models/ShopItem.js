var util = require('util');
var Model = require('../lib/Model');

/**
 * ShopItem model
 *
 * @namespace
 * @name ShopItemModel
 * @inherits Model
 * @param {Object} params
 * @constructor
 */
var ShopItemModel = function (params) {

    this._params = {
        /**
         * ID
         * @type {Number}
         */
        _id: null,


        /**
         * Price
         * @type {Number}
         */
        price: 1,

        /**
         * Type
         * @type {Number}
         */
        type: 1,

        /**
         * Count
         *
         * @type {Number}
         */
        count: 1
    };


    /**
     * Energy Type
     * @type {number}
     */
    this.TYPE_ENERGY = 1;

    /**
     * Save progress type
     *
     * @type {number}
     */
    this.TYPE_SAVE_PROGRESS = 2;

    /**
     * Fix box level
     * @type {number}
     */
    this.TYPE_FIX_BOX_LEVEL = 3;

    Model.call(this,params);
};

util.inherits(ShopItemModel, Model);

/**
 * @lends ShopItemModel
 */
ShopItemModel.prototype.extend({

});

module.exports = ShopItemModel;