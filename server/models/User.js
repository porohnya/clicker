/*global application*/
var Helper = require('../lib/Helper');
var async = require('async');
var util = require('util');
var Model = require('../lib/Model');
var BagItemModel = require('../models/BagItem');
var EffectModel = require('../models/Effect');

/**
 * User Class
 *
 * @name User
 * @namespace
 * @param {Object} params
 * @constructor
 */
var UserModel = function (params) {

    this.socket = null;

    /**
     * Timers object
     * @type {Object}
     */
    this.timers = {};

    this._params = {
        /**
         * ID
         * @type {Number}
         */
        _id: null,

        /**
         * Device UUID
         */
        uuid: null,

        /**
         * Level ID
         * @type {Number}
         */
        levelId: 1,

        /**
         * Experience
         * @type {Number}
         */
        experience: 0,

        /**
         * Box ID
         * @type {Number}
         */
        boxId: 1,

        /**
         * Fixed box level
         * @type {Number}
         */
        fixedBoxLevel: 1,

        /**
         * Money
         * @type {Number}
         */
        money:0,

        /**
         * Energy Spent
         * @type {Number}
         */
        energySpent: 0,

        /**
         * Energy extended
         *
         * @type {Number}
         */
        energyExtended: 0,

        /**
         * Energy start regeneration date
         * @type {Number}
         */
        startEnergyRegenerationDate: 0,

        /**
         * Count of total invites
         * @type {Number}
         */
        invitesTotal: 0,

        /**
         * Count of used invites
         * @type {Number}
         */
        invitesUsed: 0,

        /**
         * Unallocated drop
         * @type {{}[]}
         */
        unallocatedDrop: [],


        /**
         * Purchased items
         * @type BagItem[]
         */
        bag: [],

        /**
         * Effects on user
         * @type Effect[]
         */
        effects: []
    };
    Model.call(this, params, 'User');
};

util.inherits(UserModel, Model);

/**
 * @lends UserModel
 */
UserModel.prototype.extend({

    /**
     * Returns box model
     * @returns {BoxModel}
     */
    get box() {
        return application.storage.getModel('Box', this.boxId);
    },


    /**
     * Returns level model
     * @returns {LevelModel}
     */
    get level() {
        return application.storage.getModel('Level', this.levelId);
    },


    /**
     * Level setter
     *
     * @param {LevelModel} level
     */
    set level(level) {
        this.levelId = level._id;
    },


    /**
     * Returns is energy available flag
     *
     * @returns {boolean}
     */
    get isEnergyAvailable() {
        return (this.level.energy + this.energyExtended - this.energySpent) > 0;
    },

    /**
     * Returns total effect
     * @returns {{saveProgress: number, fixLevel: number}}
     */
    get totalEffect() {
        var totalEffect = {
            saveProgress: 0,
            fixLevel: 0
        };

        var keys = Object.keys(totalEffect);

        this.effects.forEach(function (effect) {
            keys.forEach(function (effectName) {
               if (effect[effectName]) {
                   totalEffect[effectName] += effect[effectName];
               }
            });
        });

        return totalEffect;
    },

    /**
     * Add money
     * @param {Number} money
     * @returns {number}
     */
    addMoney: function (money) {
        money = Math.round(money);
        if (isNaN(money)) {
            money = 0;
        }

        this.money += money;
        return money;
    },

    /**
     * Clear timer id
     *
     * @param {String} name
     */
    clearTimer: function (name) {
        if (!this.timers[name]) {
            return;
        }

        console.log('Timer "'+ name + '" has been removed for User #' + this._id);
        clearTimeout(this.timers[name]);
        this.timers[name] = null;
    },

    /**
     * Clears all user timers
     */
    clearTimers: function () {
        Object.keys(this.timers).forEach(function (name) {
            this.clearTimer(name)
        },this);
    },

    /**
     * Add experience
     * @returns {Number} added experience
     */
    addExperience: function (experience) {
        experience = Math.round(experience);
        var result = experience;

        //if no level up
        if (this.experience + experience < this.level.experience) {
            this.experience += experience;
            return result;
        }

        var nextLevel = null;
        Object.keys(application.storage.getModels('Level')).forEach(function (levelId) {
            var levelModel = application.storage.getModel('Level', levelId);
            if ((!nextLevel || levelModel.level < nextLevel.level) && levelModel.level > this.level.level) {
                nextLevel = levelModel;
            }
        },this);

        //if no next level
        if (!nextLevel) {
            result = this.level.experience - this.experience;
            this.experience = this.level.experience;
            return result;
        }

        var deltaExperience = experience - (this.level.experience - this.experience);
        this.level = nextLevel;
        this.experience = deltaExperience;
        this.levelUp(nextLevel);
        return result;

    },

    /**
     * Notify about user update
     */
    userUpdate: function () {
        this.notify('userUpdate', {
            user: this.toExport()
        });
    },

    /**
     * Repair Energy
     */
    repairEnergy: function () {
        console.log('------------ REPAIR ENERGY -----------');
        this.energySpent = 0;
        this.startEnergyRegenerationDate = 0;
        this.clearTimer('energyRegeneration');
        this.notify('energyRepaired', {});
        this.userUpdate();
    },


    /**
     * Start energy regeneration
     */
    startEnergyRegeneration: function () {
        console.log('START ENERGY REGENERATION');
        if (this.timers['energyRegeneration']) {
            return;
        }

        var timerEnd = 5 * 60;

        if (this.startEnergyRegenerationDate === 0 && this.energySpent > 0) {
            this.startEnergyRegenerationDate = Date.nowTS();
        } else {
            timerEnd = this.startEnergyRegenerationDate + timerEnd - Date.nowTS();
        }
        console.log('Timer Ends in ', timerEnd, 'sec');

        if (timerEnd < 0) {
            this.repairEnergy();
            return;
        }

        this.timers['energyRegeneration'] = setTimeout(this.repairEnergy.bind(this), timerEnd * 1000);
    },

    /**
     * Add energy to user
     * @param {Number} count
     */
    addEnergy: function (count) {
        console.log('add Energy',count);
        this.energySpent -= count;
        if (this.energySpent <= 0) {
            this.energySpent = 0;
            this.repairEnergy();
        }

    },

    /**
     * Add extended energy to user
     *
     * @param count
     */
    addEnergyExtended: function (count) {
        if (this.energySpent > count) {
            this.energySpent -= count;
            return;
        }

        var delta = count - this.energySpent;
        this.energySpent = 0;
        this.energyExtended += delta;
        this.repairEnergy();
    },

    /**
     * Adds invite
     * @param {Number} count
     */
    addInvite: function (count) {
        this.invitesTotal += count;
    },

    /**
     * Allocates drop by index
     * @param {Number} index
     */
    allocateDrop: function (index) {
        var drop = this.unallocatedDrop.splice(index, 1);
        if (!drop.length) {
            return;
        }
        if (drop[0].energy) {
            this.addEnergy(drop[0].energy);
        }

        if (drop[0].invite) {
            this.addInvite(drop[0].invite);
        }

        this.userUpdate();
    },


    /**
     * Un box current box
     */
    unBox: function () {
        if (!this.isEnergyAvailable) {
            return;
        }

        if (this.unallocatedDrop.length) {
            return;
        }

        if (this.energyExtended > 0) {
            this.energyExtended -= 1;
        } else {
            this.energySpent += 1;
        }

        if (this.energySpent === 1) {
            this.startEnergyRegeneration();
        }


        var result = this.box.unBox();
        var newBox = application.storage.getModel('Box', result.newBoxId);
        if (newBox.level < this.fixedBoxLevel) {
            result.newBoxId = this.box._id;
        }
        this.boxId = result.newBoxId;

        if (this.box.fixation) {
            this.fixedBoxLevel = this.box.level;
        }


        result.drops.forEach(function (dropResult) {
            if (dropResult.money) {
                this.addMoney(dropResult.money);
            }

            if (dropResult.experience) {
                this.addExperience(dropResult.experience);
            }

            if (dropResult.energy || dropResult.invite) {
                this.unallocatedDrop.push(dropResult);
            }
        }, this);

        this.notify('unBoxResult', {
            boxId: result.newBoxId,
            drops: result.drops
        });

        this.userUpdate();
    },


    /**
     * Level up notification
     * @param {LevelModel} nextLevel
     */
    levelUp: function (nextLevel) {
        this.notify('levelUp',{
            level: nextLevel._id
        });

        this.energySpent = 0;
        this.startEnergyRegenerationDate = 0;
        this.clearTimer('energyRegeneration');
    },

    /**
     * Buy item
     * @param {ShopItemModel} shopItem
     * @returns {Boolean}
     */
    buy: function (shopItem) {
        if (shopItem.price > this.money) {
            return false;
        }

        this.money -= Math.floor(shopItem.price);
        /**
        Helper.addToBag
        switch (shopItem.type) {
            case shopItem.TYPE_ENERGY:
                this.addEnergyExtended(shopItem.count);
                break;
        }*/

        this.addToBag(shopItem);

        return true;
    },

    /**
     * Use bag item
     *
     * @param {BagItemModel} bagItem
     */
    useBagItem: function (bagItem) {

        var shopItem = bagItem.shopItem;
        switch (shopItem.type) {
            case shopItem.TYPE_ENERGY:
                this.addEnergyExtended(shopItem.count);
                break;
        }

        if (bagItem.count > 1) {
            bagItem.count -= 1;
        } else {
            this.bag.removeElement(bagItem);
        }
        this.userUpdate();
    },

    /**
     * Add shopItem to bag
     * @param shopItem
     */
    addToBag: function (shopItem) {
        var foundBagItem = null;
        this.bag.some(function (bagItem) {
            if (bagItem.shopItem._id === shopItem._id) {
                foundBagItem = bagItem;
                return true;
            }
        });

        if (!foundBagItem) {
            this.bag.push(new BagItemModel({
                shopItemId: shopItem._id,
                count: 1
            }));
        } else {
            foundBagItem.count += 1;
        }

        this.userUpdate();
    },

    /**
     * Bind user to socket
     */
    bind: function () {
        if (!this.socket) {
            console.error('cannot bind null socket');
            return;
        }

        var self = this;

        this.socket.on('message', function (message) {
            console.log('Message received:', message);

            if (!message.data || !message.action) {
                self.notify('error', 'action or data not found');
                return;
            }

            if (!self.handlers[message.action]) {
                self.notify('error', 'unknown action');
                return;
            }

            self.handlers[message.action].call(self, message.data);

        });
    },

    /**
     * Send message to client
     * @param {String} action
     * @param {Object} data
     */
    notify: function (action, data) {
        data = data || {};
        this.socket.emit('message', {action: action, data: data, timestamp: Date.nowTS()});
    },

    /**
     * Disconnection handler
     */
    onDisconnect: function () {
        var id = this._id;
        if (!id) {
            console.log('[TRANSPORT] Unauthorized socket disconnected');
            return;
        }

        this.socket = null;

        console.log('[TRANSPORT] User ', id, ' has disconnected. He will be removed from storage in 5 sec');
        setTimeout(function () {
            console.log('[TRANSPORT] User ', id, ' has been removed form storage');
            this.clearTimers();
            this.save(function () {
                application.storage.removeUser(id);
            });
        }.bind(this), 1000);

    },



    /**
     * Update params
     * @param {Object} params
     */
    updateParams: function (params) {
        Object.keys(params).forEach(function (key) {
            var data = params[key];
            switch (key) {
                case 'bag':
                    data = [];
                    params[key].forEach(function (element) {
                        data.push(new BagItemModel(element));
                    });
                    break;
                case 'effects':
                    data = [];
                    params[key].forEach(function (element) {
                        data.push(new EffectModel(element));
                    });
            }

            this.setterFunction(key, data);
        }, this);
    },


    /**
     * Save user to database
     * @param {Function} callback
     */
    save: function (callback) {
        callback = callback || function () {};
        var data = this.toExport();
        data.bag = [];
        data.effects = [];

        this.bag.forEach(function (item) {
            data.bag.push(item.toExport());
        });

        this.effects.forEach(function (item) {
            data.effects.push(item.toExport());
        });

        application.mongo.save(this.modelName, data, function () {
            callback();
        });
    },


    handlers: {


        /**
         * Returns models for client
         */
        getDictionary: function () {
            this.notify('dictionary', {
                models: application.storage.exportModels()
            });
        },


        /**
         * UnBox current command
         */
        unBox: function () {
            this.unBox();
        },


        /**
         * Returns player info
         */
        getPlayerInfo: function () {
            this.notify('playerInfo', this.toExport());
        },


        /**
         * Collects unallocated Drop
         */
        collectDrop: function (data) {
            data.index = parseInt(data.index, 10) || 0;
            if (isNaN(data.index)) {
                data.index = 0;
            }

            if (!this.unallocatedDrop.length) {
                return this.notify('error','unallocated drop not found');
            }

            var dropContainer = this.unallocatedDrop[data.index];
            if (!dropContainer) {
                return this.notify('error','wrong unallocated drop index');
            }

            this.allocateDrop(data.index);
        },

        /**
         * Use item from bag
         * @param {Object} data
         * @returns {*}
         */
        useBag: function (data) {
            var index = parseInt(data.index, 10) || 0;

            if (isNaN(index)) {
                return this.notify('error', 'wrong params');
            }

            var bagItem = this.bag[index];
            if (!bagItem) {
                return this.notify('error', 'wrong params');
            }

            this.useBagItem(bagItem);
        },

        /**
         * Buy shop item
         * @param {Object} data
         * @returns {*}
         */
        buy: function (data) {
            var modelId = parseInt(data.shopItemId, 10) || 0;
            if (isNaN(modelId)) {
                return this.notify('error', 'wrong params');
            }

            var shopItem = application.storage.getModel('ShopItem', modelId);
            if (!shopItem) {
                return this.notify('error', 'shop item not found');
            }

            if (!this.buy(shopItem)) {
                return this.notify('error', 'not enough money');
            }

            this.userUpdate();

        },

        /**
         * User authorization
         *
         * @param {Object} data
         */
        auth : function (data) {
            var self = this;
            if (!data.uuid) {
                return;
            }

            var lockName = "auth-" + data.uuid;
            application.lock(lockName, function (releaseLock) {
                async.waterfall([
                    function (next) {
                        Helper.getUserByUUID(data.uuid, next);
                    },
                    function (userData, next) {
                        if (!userData) {
                            next('USER_NOT_REGISTERED');
                            return;
                        }
                        next(null, userData);

                    },
                    function (userData, next) {
                        var user = application.storage.getUser(userData._id);

                        if (!user) {
                            self.updateParams(userData);
                            self.socket._userId = self._id;
                            console.log('User not found in the storage. adding');
                            self.startEnergyRegeneration();
                            application.storage.addUser(self);
                        } else {
                            console.log('User has concurrent in the storage. disconnecting');
                            self.notify('authorized', {
                                success: false,
                                msg: 'CONCURRENT_CONNECTION'
                            });
                            self.socket.disconnect();
                        }

                        next();
                    }
                ], function (error) {
                    self.notify('authorized', {
                        success: !(error),
                        msg: error
                    });

                    releaseLock();
                });
            });
        }

    }
});


module.exports = UserModel;