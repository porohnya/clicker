require('./overrides');
var Application = require('./lib/Application');
var env = require('./env');
var config = require('./config')[env];
var args = process.argv.slice(2);
var startArgs = {};
if (args.length) {
	args.forEach(function (el) {
		var ar = el.split(',');
		startArgs[ar[0]] = ar[1] || true;
	});
}
/**
 * @name application
 * @namespace
 * @type {Application}
 */
global['application'] = new Application(config, startArgs);
application.initialize();