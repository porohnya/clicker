/**
 * Summarize all properties of one object with another
 *
 * @param {Object} from
 * @param {Boolean} applyNewProps
 * @return Object
 */
Object.merge = function(dest, from, applyNewProps) {
    applyNewProps = applyNewProps || false;
    var props = Object.getOwnPropertyNames(from);

    props.forEach(function(name) {
        var value = from[name];
        if (dest[name] != undefined) {
            dest[name] += value;
        } else if (applyNewProps) {
            dest[name] = value;
        }
    });
    return dest;
};

/**
 * Add all properties of one object to another
 *
 * @param {Object} from
 * @return Object
 */
Object.defineProperty(Object.prototype, "extend", {
    enumerable: false,
    value: function(from) {
        var dest = this;
        var props = Object.getOwnPropertyNames(from);

        for (name in from) {
            if (props.indexOf(name) != -1) {
                var description = Object.getOwnPropertyDescriptor(from, name);
                Object.defineProperty(dest, name, description);
            }
        }

        return this;
    }
});

/**
 * Returns array that contains only unique (with respect to ===)
 * elements.
 *
 * @return {Array}
 */
Object.defineProperty(Array.prototype, "unique", {
    enumerable : false,
    value      : function (arr) {
        var result = [],
            len    = this.length;

        for(var i = 0; i<len; i++) {
            for(var j = i+1; j<len; j++) {
                // If this[i] is found later in the array
                if (arr[i] === arr[j])
                    j = ++i;
            }
            result.push(arr[i]);
        }
        return result;
    }
});

/**
 * Returns object in the array that matches given predicate.
 *
 * @param {Function} predicate
 * @return {Object}
 */
Object.defineProperty(Array.prototype, "findElement", {
    enumerable : false,
    value      : function (predicate) {
        var result = null;

        this.some(function (element) {
            if (predicate(element)) {
                result = element;
                return true;
            }
            return false;
        });

        return result;
    }
});

/**
 * Removes element from array. Checks element match with ===
 *
 * @param {Object} element
 * @return {Array}
 */
Object.defineProperty(Array.prototype, "removeElement", {
    enumerable : false,
    value      : function (element) {
        var result = this.filter(function (elem) {
            return elem !== element;
        });
        //Prepare argument list for apply.
        result.unshift(this.length);
        result.unshift(0);

        this.splice.apply(this, result);

        return this;
    }
});

/**
 * Shuffle an array
 *
 * @return {Array}
 */
Object.defineProperty(Array.prototype, "shuffle", {
    enumerable: false,
    value: function() {
        for (var j, x, i = this.length; i; j = parseInt(Math.random() * i), x = this[--i], this[i] = this[j], this[j] = x);
        return this;
    }
});

/**
 * Make a string's first character uppercase
 *
 * @return String
 */
Object.defineProperty(Object.prototype, "ucfirst", {
    enumerable: false,
    value: function() {
        var f = this[0].toUpperCase();
        return this.replace(this[0], f);
    }
});

/**
 * Make a string's first character lowercase
 *
 * @return String
 */
Object.defineProperty(Object.prototype, "lcfirst", {
    enumerable: false,
    value: function() {
        var f = this[0].toLowerCase();
        return this.replace(this[0], f);
    }
});

/**
 * Returns formatted date
 *
 * @return String
 */
Object.defineProperty(Date.prototype, "toFormatString", {
    enumerable: false,
    value: function() {
        var d  = this.getDate();
        var day = (d < 10) ? '0' + d : d;
        var m = this.getMonth() + 1;
        var month = (m < 10) ? '0' + m : m;
        var yy = this.getYear();
        var year = (yy < 1000) ? yy + 1900 : yy;

        var h = this.getHours();
        var hours = (h < 10) ? '0' + h : h;
        var min = this.getMinutes();
        var minutes = (min < 10) ? '0' + min : min;
        var sec = this.getSeconds();
        var seconds = (sec < 10) ? '0' + sec : sec;
        return day + "-" + month + "-" + year + ' ' + hours + ':' + minutes + ':' + seconds;
    }
});

/**
 * Returns current timestamp in seconds
 *
 * @return String
 */
Date.nowTS = function() {
    var now = Date.now();
    var nowStr = String(now);
    return Number(nowStr.slice(0, nowStr.length - 3));
};