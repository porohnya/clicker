module.exports = {
    development: {
        server: {
            port: 8080
        },
        mongo: {
            host: 'localhost',
            database: 'clicker_dev',
            port: 27017
        },
        redis: {
            host: 'localhost',
            port: 6379
        },
        models: [
            {name: "Box"},
            {name: "Level"},
            {name: "Drop"},
            {name: "ShopItem"}
        ]
    },
    production: {
        server: {
            port: 80
        },
        mongo: {
            host: 'localhost',
            database: 'clicker_production',
            port: 27017
        },
        redis: {
            host: 'localhost',
            port: 6379
        },
        models: [
            {name: "Box"},
            {name: "Level"},
            {name: "Drop"},
            {name: "ShopItem"}
        ]
    }
};
