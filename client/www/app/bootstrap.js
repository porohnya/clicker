require.config({
    urlArgs: "bust=" + (new Date()).getTime(),
    paths: {
        async: '../lib/async',
        text: '../lib/text',
        fakeapi: '../lib/fakeapi',
        socketio: '../lib/socket.io.min',
        phonegap: 'phonegap',
        pixi: '../lib/pixi.dev',
        extend: '../lib/extend',
        EventEmitter: '../lib/eventEmitter.min',

        //Main config
        'config' : '../app/config'
    }
});


require(
    ['app', 'config'],
    function (App, config) {
        var env = 'production';
        var hashParams = App.getHashParams();
        if (hashParams.env && config[hashParams.env]) {
            env = hashParams.env;
        }

        window.App = App;
        App.initialize(config[env]);
    }
);