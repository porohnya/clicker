define(
    'config',
    [],
    function () {
        return {
            development: {
                socket: 'http://localhost:8080'
            },
            production: {
                socket: 'http://box.porokhnya.com:80'
            }
        }
    }
);