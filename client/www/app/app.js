define(
    'app',
    ['common','modules/transport',
     'modules/controller',
     'modules/layout',
     'modules/dictionary',
     'modules/user',
     'models/box',
     'models/shopItem',
     'models/level',
     'async', 'fakeapi', 'pixi'],
    function (Common, TransportClass,
              ControllerClass,
              LayoutClass,
              DictionaryClass,
              UserClass,
              //Models
              BoxModel,
              ShopItemModel,
              LevelModel,
              //Etc
              async) {

        var App = function () {
            /**
             * Is App running on device
             * @type {boolean}
             */
            this.isDeviceMode = this._detectDeviceMode();

            /**
             * Transport
             * @type {transportClass}
             */
            this.transport = null;


            /**
             * Controller
             * @type {controllerClass}
             */
            this.controller = null;

            /**
             * User
             * @type {UserClass}
             */
            this.user = new UserClass({isReady: false});

            /**
             * Layout
             * @type {LayoutClass}
             */
            this.layout = new LayoutClass();

            /**
             * Dictionary
             * @type {DictionaryClass}
             */
            this.dictionary = new DictionaryClass();

            /**
             * List of Model classes
             * @type {Object}
             */
            this.modelClasses = {
                Box: BoxModel,
                Level: LevelModel,
                ShopItem: ShopItemModel
            };

            /**
             * Time Difference with server
             *
             * @type {number}
             */
            this.timeDiff = 0;


        };

        /**
         * @lends App
         */
        App.prototype.extend({

            /**
             * Start Application
             * Fires when everything is loaded and connection established
             */
            start: function () {
                console.log('application started.');
                console.log('Hash Params:', JSON.stringify(this.getHashParams()));
                console.log('Config: ' +  JSON.stringify(this.config));
            },

            /**
             * Returns server timestamp
             * @returns {number}
             */
            nowTS: function () {
                return Date.nowTS() + this.timeDiff;
            },


            /**
             * Application initialization
             * @param {Object} config
             */
            initialize: function (config) {
                console.log('APPLICATION INITIALIZE');
                console.log('Device mode isDevice', this.isDeviceMode);
                var self = this;
                this.config = config;

                async.series([
                    function (next) {
                        if (!self.isDeviceMode) {
                            return next();
                        }
                        document.addEventListener("deviceready", function () {
                            console.log('DEVICE READY');
                            console.log('Window Device: '  + JSON.stringify(window.device));
                            next();
                        });
                    },
                    function (next) {
                        this.layout.loadAssets(next);
                    }.bind(this),
                    function (next) {
                        this.controller = new ControllerClass(this);
                        this.transport = new TransportClass(config.socket, this._dispatch.bind(this));
                        this.transport.connect(next);
                    }.bind(this),
                    function (next) {
                        this.send('getDictionary', {});

                        async.parallel([
                            function (cb) {
                                this.dictionary.on('ready', cb);
                            }.bind(this),
                            function (cb) {
                                this.user.on('ready', cb);
                            }.bind(this)
                        ], next)

                    }.bind(this),
                    function (next) {
                        this.layout.init();
                        next();
                    }.bind(this)
                ], function (error) {
                    if (error) {
                        console.log(error);
                        return alert(error);
                    }

                    console.log('START APPLICATION');
                    self.start();
                });
            },

            /**
             * Send data via websocket
             * @param {String} action
             * @param {Object} params
             */
            send: function (action, params) {
                console.log('%c' + action, 'background-color: green; padding: 1px; color: white', params);
                this.transport.send(action, params);
            },

            /**
             * Dispatching server event
             * @param {Object} request
             * @private
             */
            _dispatch: function (request) {
                var action = request.action || 'default';
                action += 'Action';
                console.log('%c' + action, 'background-color: blue; padding: 1px; color: white', request.data);
                //Fix time difference

                this.timeDiff = request.timestamp - Date.nowTS();

                if (this.controller[action]) {
                    this.controller[action].call(this, request.data);
                } else {
                    this.controller.defaultAction.call(this, request.data);
                }
            },

            /**
             * Detect if app running on device
             * @returns {boolean}
             * @private
             */
            _detectDeviceMode: function () {
                return document.URL.indexOf("http://") === -1;
            },

            /**
             * Returns location hash params as Object
             * @returns {Object}
             */
            getHashParams: function () {
                var result = {};
                var hash = document.location.hash;
                if (!hash) {
                    return result;
                }

                hash = hash.replace('#','');
                hash.split('&').forEach(function (pair) {
                    var pairArr = pair.split('=');
                    result[pairArr[0]] = pairArr[1];
                });

                return result;
            }

        });

        return new App();
    }
);