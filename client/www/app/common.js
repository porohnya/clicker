define ('common',[],
    function () {
        Object.defineProperty(Object.prototype, "extend", {
            enumerable: false,
            value: function(from) {
                var dest = this;
                var props = Object.getOwnPropertyNames(from);

                for (name in from) {
                    if (props.indexOf(name) != -1) {
                        var description = Object.getOwnPropertyDescriptor(from, name);
                        Object.defineProperty(dest, name, description);
                    }
                }

                return this;
            }
        });


        /**
         * Returns current timestamp in seconds
         *
         * @return String
         */
        Date.nowTS = function() {
            var now = Date.now();
            var nowStr = String(now);
            return Number(nowStr.slice(0, nowStr.length - 3));
        };



        /**
         * Returns random integer
         * @param {Number} min
         * @param {Number} max
         * @returns {Number}
         */
        window.getRandomInt = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }
);