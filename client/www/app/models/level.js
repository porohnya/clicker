define(
    'models/level',
    [],
    function () {

        /**
         * @name LevelModel
         * @namespace
         * @constructor
         */
        var LevelModel = function (params) {
            this.extend(params);
        };

        /**
         * @lends LevelModel
         */
        LevelModel.prototype.extend({

            /**
             * Get current level experience
             * @returns {number}
             */
            get experience() {
                return Math.pow(this.level, 2) * 50 + 100;
            },

            /**
             * Get current level energy
             * @returns {number}
             */
            get energy() {
                return Math.floor(49 + Math.pow(this.level, 1.5688));
            },

            /**
             * Get current level money reward
             * return {number}
             */
            get moneyReward() {
                return Math.floor(Math.pow(this.level, 1.5) * 100);
            }

        });


        return LevelModel;
    }
);