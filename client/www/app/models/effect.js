define(
    'models/effect',
    [],
    function () {

        /**
         * @name EffectModel
         * @namespace
         * @constructor
         */
        var EffectModel = function (params) {
            this.extend(params);
        };


        /**
         * @lends EffectModel
         */
        EffectModel.prototype.extend({

            /**
             * Returns Effect
             *
             * @returns {EffectModel}
             */
            get shopItem() {
                return App.dictionary.getModel('Effect', this.shopItemId);
            }
        });


        return EffectModel;
    }
);