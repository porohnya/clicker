define(
    'models/shopItem',
    [],
    function () {

        /**
         * @name ShopItemModel
         * @namespace
         * @constructor
         */
        var ShopItemModel = function (params) {
            this.extend(params);
        };


        /**
         * @lends ShopItemModel
         */
        ShopItemModel.prototype.extend({

            /**
             * Type Energy
             */
            TYPE_ENERGY: 1,

            /**
             * Type save progress
             */
            TYPE_SAVE_PROGRESS: 2,

            /**
             * Type fix box level
             */
            TYPE_FIX_BOX_LEVEL: 3
        });


        return ShopItemModel;
    }
);