define(
    'models/box',
    [],
    function () {

        /**
         * @name BoxModel
         * @namespace
         * @constructor
         */
        var BoxModel = function (params) {
            this.extend(params);
        };


        return BoxModel;
    }
);