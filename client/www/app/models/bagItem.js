define(
    'models/bagItem',
    [],
    function () {

        /**
         * @name BagItemModel
         * @namespace
         * @constructor
         */
        var BagItemModel = function (params) {
            this.extend(params);
        };


        /**
         * @lends BagItemModel
         */
        BagItemModel.prototype.extend({

            /**
             * Returns shopItem
             *
             * @returns {ShopItemModel}
             */
            get shopItem() {
                return App.dictionary.getModel('ShopItem', this.shopItemId);
            }
        });


        return BagItemModel;
    }
);