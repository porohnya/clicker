define(
    'components/shopItemList',
    ['components/abstract', 'pixi'],
    function (ComponentAbstractClass, PIXI) {

        /**
         * Component loot
         *
         * @name ComponentShopItemListClass
         * @namespace
         * @constructor
         */
        var ComponentShopItemListClass = function (scene) {
            ComponentAbstractClass.call(this, scene);


            this.assetList = {
                base: {
                    url: 'img/white_pixel.png',
                    texture: null
                },
                button : {
                    url: 'img/coloredButtons.png',
                    texture: null,
                    frameIdle: {
                        x: 0,
                        y: 96,
                        width: 82,
                        height: 32
                    },
                    framePressed: {
                        x: 82,
                        y: 96,
                        width: 82,
                        height: 32
                    }
                }
            };




        };


        ComponentShopItemListClass.prototype = Object.create(ComponentAbstractClass.prototype);
        ComponentShopItemListClass.prototype.constructor = ComponentAbstractClass;


        /**
         * @lends ComponentShopItemListClass
         */
        ComponentShopItemListClass.prototype.extend({


            /**
             * Init function
             */
            init: function () {

                var shopItems = App.dictionary.getModelList('ShopItem');

                for (var index in shopItems) {
                    var self = this;
                    var currId = shopItems[index]._id;
                    var currBlock = self['shopItemBlock' + currId] = new PIXI.DisplayObjectContainer();
                    this.container.addChild(currBlock);

                    currBlock.shopItemModel = shopItems[index];

                    var blockParts = currBlock._components = {};

                    blockParts.base = new PIXI.Sprite(self.assetList.base.texture);
                    currBlock.addChild(blockParts.base);
                    blockParts.base.width = 300;
                    blockParts.base.height = 80;

                    blockParts.descriptionText = new PIXI.Text('Количество: ' +  + shopItems[index].count + '\n' + 'Цена: ' + shopItems[index].price, {font:"20px Arial", fill:"black"});
                    currBlock.addChild(blockParts.descriptionText);

                    blockParts.buttonContainer = new PIXI.DisplayObjectContainer();
                    currBlock.addChild(blockParts.buttonContainer);
                    blockParts.buttonContainer.setInteractive(true);

                    var texture = new PIXI.Texture(self.assetList.button.texture, self.assetList.button.frameIdle);
                    blockParts.buttonSprite = new PIXI.Sprite(texture);
                    blockParts.buttonContainer.addChild(blockParts.buttonSprite);

                    blockParts.buttonText = new PIXI.Text('Купить', {
                        font:"17px Arial",
                        fill:"white",
                        stroke : 'black',
                        strokeThickness : 3
                    });
                    blockParts.buttonContainer.addChild(blockParts.buttonText);

                    //Positioning
                    self.setCenterPosition.call(blockParts.descriptionText);
                    blockParts.descriptionText.x = blockParts.descriptionText.y;

                    self.setCenterPosition.call(blockParts.buttonContainer, currBlock);
                    self.setPercentagePosition.call(blockParts.buttonContainer, 1, '', currBlock);
                    blockParts.buttonContainer.x -= (blockParts.buttonContainer.width + blockParts.buttonContainer.y);

                    self.setCenterPosition.call(blockParts.buttonText);

                    currBlock.x = 0;
                    currBlock.y = (currBlock.height + 10) * (currId -1);

                    //Handlers
                    blockParts.buttonContainer.click = blockParts.buttonContainer.tap = function (e) {
                        App.user.buy(e.target.parent.shopItemModel);
                    };

                    blockParts.buttonContainer.mousedown = blockParts.buttonContainer.touchstart = function (eventData) {
                        eventData.target.parent._components.buttonSprite.texture.setFrame(self.assetList.button.framePressed);
                    };

                    blockParts.buttonContainer.mouseup = blockParts.buttonContainer.touchend = function (eventData) {
                        eventData.target.parent._components.buttonSprite.texture.setFrame(self.assetList.button.frameIdle);
                    };
                }

                this.setCenterPosition();
            }


        });


        return ComponentShopItemListClass;
    }
);