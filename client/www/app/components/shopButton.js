define(
    'components/shopButton',
    ['components/abstract', 'pixi', 'async'],
    function (ComponentAbstractClass, PIXI) {

        /**
         * Component shopButton
         *
         * @name ComponentShopButton
         * @namespace
         * @constructor
         */
        var ComponentShopButtonClass = function () {
            ComponentAbstractClass.call(this);

            this.assetList = {
                button : {
                    url: 'img/defaultButton.png',
                    texture: null,
                    frameIdle: {
                        x: 3,
                        y: 0,
                        width: 249,
                        height: 47
                    },
                    framePressed: {
                        x: 3,
                        y: 99,
                        width: 249,
                        height: 47
                    }
                }
            };
        };


        ComponentShopButtonClass.prototype = Object.create(ComponentAbstractClass.prototype);
        ComponentShopButtonClass.prototype.constructor = ComponentAbstractClass;


        /**
         * @lends ComponentShopButtonClass
         */
        ComponentShopButtonClass.prototype.extend({

            /**
             * Initialize function
             */
            init: function () {
                this.on('tap', function () {
                    App.layout.showScene('shop')
                });
                this.on('touchstart', this.onTouchStart.bind(this));
                this.on('touchend', this.onTouchEnd.bind(this));

                this.button = new PIXI.Sprite(this.assetList.button.texture);
                this.button.texture.setFrame(this.assetList.button.frameIdle);

                this.container.addChild(this.button);

                this.setPercentagePosition(0, 1);
                this.x += 10;

                this.y -= this.height;
                this.y -= 50;

                this.text = new PIXI.Text('Shop', {font:"20px Arial", fill:"white"});
                this.container.addChild(this.text);

                this.setCenterPosition.call(this.text);
            },

            /**
             * TouchStart animation
             */
            onTouchStart: function () {
                this.button.texture.setFrame(this.assetList.button.framePressed);
            },

            /**
             * TouchEnd animation
             */
            onTouchEnd: function () {
                this.button.texture.setFrame(this.assetList.button.frameIdle);
            }

        });


        return ComponentShopButtonClass;
    }
);