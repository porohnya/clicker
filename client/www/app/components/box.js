define(
    'components/box',
    ['components/abstract', 'pixi', 'async'],
    function (ComponentAbstractClass, PIXI) {

        /**
         * Component box
         *
         * @name ComponentBoxClass
         * @namespace
         * @constructor
         */
        var ComponentBoxClass = function () {
            ComponentAbstractClass.call(this);


            this.assetList = {
                boxImage : {
                    url: 'img/box.png',
                    texture: null
                }
            };
        };


        ComponentBoxClass.prototype = Object.create(ComponentAbstractClass.prototype);
        ComponentBoxClass.prototype.constructor = ComponentAbstractClass;


        /**
         * @lends ComponentBoxClass
         */
        ComponentBoxClass.prototype.extend({

            /**
             * Initialize function
             */
            init: function () {
                this.on('tap', App.user.unBox.bind(App.user));
                this.on('touchstart', this.onTouchStart.bind(this));
                this.on('touchend', this.onTouchEnd.bind(this));

                this.box = new PIXI.Sprite(this.assetList.boxImage.texture);
                this.box.width = this.box.height = 200;
                this.container.addChild(this.box);
                this.setCenterPosition();

            },

            /**
             * TouchStart animation
             */
            onTouchStart: function () {
                this.x += 10;
                this.y += 10;
            },

            /**
             * TouchEnd animation
             */
            onTouchEnd: function () {
                this.x -= 10;
                this.y -= 10;
            }

        });


        return ComponentBoxClass;
    }
);