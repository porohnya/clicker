/**
 * Created by Titus on 12.10.14.
 */
define(
    'components/backButton',
    ['components/abstract', 'pixi', 'async'],
    function (ComponentAbstractClass, PIXI) {
        /**
         * Component backButton
         *
         * @name ComponentBackButton
         * @namespace
         * @constructor
         */
        var ComponentBackButtonClass = function () {
            ComponentAbstractClass.call(this);

            this.assetList = {
                button : {
                    url: 'img/defaultButton.png',
                    texture: null,
                    frameIdle: {
                        x: 3,
                        y: 0,
                        width: 249,
                        height: 47
                    },
                    framePressed: {
                        x: 3,
                        y: 99,
                        width: 249,
                        height: 47
                    }
                }
            };
        };


        ComponentBackButtonClass.prototype = Object.create(ComponentAbstractClass.prototype);
        ComponentBackButtonClass.prototype.constructor = ComponentAbstractClass;


        /**
         * @lends ComponentBackButtonClass
         */
        ComponentBackButtonClass.prototype.extend({

            /**
             * Initialize function
             */
            init: function () {
                this.on('tap', function() {
                    App.layout.showScene('main');
                });
                this.on('touchstart', this.onTouchStart.bind(this));
                this.on('touchend', this.onTouchEnd.bind(this));

                this.button = new PIXI.Sprite(new PIXI.Texture(this.assetList.button.texture, this.assetList.button.frameIdle));

                this.container.addChild(this.button);

                this.setPercentagePosition(0, 1);
                this.x += 10;

                this.y -= this.height;
                this.y -= 50;

                this.text = new PIXI.Text('Back', {font:"20px Arial", fill:"white"});
                this.container.addChild(this.text);

                this.setCenterPosition.call(this.text);
            },

            /**
             * TouchStart animation
             */
            onTouchStart: function () {
                this.button.setTexture(new PIXI.Texture(this.assetList.button.texture, this.assetList.button.framePressed));
            },

            /**
             * TouchEnd animation
             */
            onTouchEnd: function () {
                this.button.setTexture(new PIXI.Texture(this.assetList.button.texture, this.assetList.button.frameIdle));
            }

        });


        return ComponentBackButtonClass;
    }
);