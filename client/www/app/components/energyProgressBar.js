/**
 * Created by Titus on 01.10.14.
 */
define(
    'components/energyProgressBar',
    ['components/abstract', 'pixi'],
    function (ComponentAbstractClass, PIXI) {
        var ComponentEnergyProgressBarClass = function () {
            ComponentAbstractClass.call(this);

            this.assetList = {
                base : {
                    url: 'img/black_pixel.png',
                    texture: null,
                    width : 354,
                    height : 49
                },
                empty : {
                    url: 'img/white_pixel.png',
                    texture: null,
                    width : 350,
                    height : 45
                },
                filled : {
                    url: 'img/yellow_pixel.png',
                    texture: null
                }
            }

        };

        ComponentEnergyProgressBarClass.prototype = Object.create(ComponentAbstractClass.prototype);
        ComponentEnergyProgressBarClass.prototype.constructor = ComponentAbstractClass;

        /**
         * @lends ComponentDefaultProgressBarClass
         */
        ComponentEnergyProgressBarClass.prototype.extend({

            /**
             * Initialize function
             */
            init: function () {
                //Background
                this.base = new PIXI.Sprite(this.assetList.base.texture);
                this.container.addChild(this.base);

                this.base.width = this.assetList.base.width;
                this.base.height = this.assetList.base.height;

                //Empty part
                this.empty = new PIXI.Sprite(this.assetList.empty.texture);
                this.container.addChild(this.empty);

                this.empty.width = this.assetList.empty.width;
                this.empty.height = this.assetList.empty.height;

                this.setCenterPosition.call(this.empty);

                //Filled part
                this.filled = new PIXI.Sprite(this.assetList.filled.texture);
                this.container.addChild(this.filled);

                this.filled.width = 0;
                this.filled.height = this.assetList.filled.height || this.assetList.empty.height;

                this.setCenterPosition.call(this.filled);
                this.filled.x = this.empty.x;

                //Text
                this.text = new PIXI.Text('', {
                    font : '20px Arial',
                    fill : 'white',
                    stroke : 'black',
                    strokeThickness : 4
                });
                this.container.addChild(this.text);

                //Container
                this.setCenterPosition();
                this.y = 20;

                this.y = 80;
            },

            getEnergyInfo: function () {
                var result = {
                    percent: Math.floor( (App.user.level.energy - App.user.energySpent) / App.user.level.energy * 100 ) / 100,
                    text: 'Energy: ' + App.user.energyAvailable + '/' + App.user.level.energy
                }

                if (App.user.secondsToRestoreEnergy) {
                    var clock = App.user.timerToRestoreEnergy;
                    result.text += ' (' + clock.minutes + ':' + clock.seconds + ')';
                }

                return result
            },

            /**
             * Render method
             */
            onBeforeRender: function() {
                this.setCenterPosition.call(this.text);

                this.filled.width = this.getEnergyInfo().percent * this.empty.width;
                this.text.setText(this.getEnergyInfo().text);
            }

        });

        return ComponentEnergyProgressBarClass;
    }
)