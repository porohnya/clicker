define(
    'components/loot',
    ['components/abstract', 'pixi'],
    function (ComponentAbstractClass, PIXI) {

        /**
         * Component loot
         *
         * @name ComponentLootClass
         * @namespace
         * @constructor
         */
        var ComponentLootClass = function (scene) {
            ComponentAbstractClass.call(this, scene);


            this.assetList = {
                energyIcon : {
                    url: 'img/energy.png',
                    texture: null
                },
                inviteIcon: {
                    url: 'img/invite.png',
                    texture: null
                }

            };



            this.textOffsetX = 70;
            this.textOffsetY = 5;
        };


        ComponentLootClass.prototype = Object.create(ComponentAbstractClass.prototype);
        ComponentLootClass.prototype.constructor = ComponentAbstractClass;


        /**
         * @lends ComponentLootClass
         */
        ComponentLootClass.prototype.extend({


            /**
             * Init function
             */
            init: function () {
                App.user.on('update', function () {
                    this.animate(App.user.unallocatedDrop);
                }.bind(this));
            },

            /**
             * Adds Blocks to this scene
             *
             * @param {String} name Block name
             * @param {Number} count Count
             * @param {Number} index
             * @private
             */
            _addBlock: function (name, count, index) {
                var block   = new PIXI.DisplayObjectContainer();
                var icon    = new PIXI.Sprite(this.assetList[name + 'Icon'].texture);
                var text    = new PIXI.Text("+" + count.toString(), {font:"50px Arial", fill:"black"});
                icon.width = icon.height = 64;
                block.visible = true;
                block.addChild(icon);
                block.addChild(text);
                block.setInteractive(true);
                text.position.x += this.textOffsetX;
                text.position.y += this.textOffsetY;
                this.container.addChild(block);
                block.position.y += index * 100;
                block.click = block.tap = function () {
                    App.user.collectDrop(index);
                };

            },


            /**
             * Animate drop
             *
             * @param {Object} drops
             */
            animate: function (drops) {
                this.container.children.length = 0;
                drops.forEach(function (drop, index) {
                    var dropType = null;
                    switch (true) {
                        case drop.energy > 0:
                            dropType = 'energy';
                            break;
                        case drop.invite > 0:
                            dropType = 'invite';
                            break;
                    }

                    this._addBlock(dropType, drop[dropType], index);
                },this);
                var boxBounds = this.scene.getComponentBounds('box');
                this.container.x = boxBounds.x + 250;
                this.container.y = boxBounds.y;
                this.container.visible = true;
            }

        });


        return ComponentLootClass;
    }
);