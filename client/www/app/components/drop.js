define(
    'components/drop',
    ['components/abstract', 'pixi', 'async'],
    function (ComponentAbstractClass, PIXI, async) {

        /**
         * Component drop
         *
         * @name ComponentDropClass
         * @namespace
         * @constructor
         */
        var ComponentDropClass = function (scene) {
            ComponentAbstractClass.call(this, scene);


            this.assetList = {
                moneyIcon : {
                    url: 'img/coin.png',
                    texture: null
                },
                experienceIcon: {
                    url: 'img/exp.png',
                    texture: null
                }

            };

            this.width = window.innerWidth;
            this.height = window.innerHeight;

            this.textOffsetX = 40;
            this.textOffsetY = 5;

            this.zIndex = 100;
        };


        ComponentDropClass.prototype = Object.create(ComponentAbstractClass.prototype);
        ComponentDropClass.prototype.constructor = ComponentAbstractClass;


        /**
         * @lends ComponentDropClass
         */
        ComponentDropClass.prototype.extend({

            /**
             * Initialize function
             */
            init: function () {
                this._addBlock('money');
                this._addBlock('experience');
            },


            /**
             * Adds Blocks to this scene
             *
             * @param {String} name Block name
             * @private
             */
            _addBlock: function (name) {
                var block   = new PIXI.DisplayObjectContainer();
                var icon    = new PIXI.Sprite(this.assetList[name + 'Icon'].texture);
                var text    = new PIXI.Text("0", {font:"20px Arial", fill:"black"});
                block.visible = false;
                block.addChild(icon);
                block.addChild(text);
                text.position.x += this.textOffsetX;
                text.position.y += this.textOffsetY;
                this.container.addChild(block);
                this[name + 'Block']    = block;
                this[name + 'Text']     = text;
                this[name + 'Icon']     = icon;
            },


            /**
             * Animate drop
             *
             * @param {String} name
             * @param {Number} count
             */
            animate: function (name, count) {
                var block  = this[name + 'Block'];
                var text    = this[name + 'Text'];
                if (!block) {
                    throw new Error('Unknown resourceID');
                }

                if (block.timerAnimationID) {
                    clearInterval(block.timerAnimationID);
                    block.timerAnimationID = null;
                }

                var boxBounds = this.scene.getComponentBounds('box');

                block.position.x = boxBounds.x;
                block.position.y = boxBounds.y;
                block.visible = true;
                text.setText(count);
                block.timerAnimationID = setInterval(function () {
                    block.position.y -= 2;
                    block.position.y -= 2;
                    if (block.position.y < 0) {
                        block.visible = false;
                        clearInterval(block.timerAnimationID);
                        block.timerAnimationID = null;
                    }
                }.bind(this), 15);
            }

        });


        return ComponentDropClass;
    }
);