define(
    'components/expProgressBar',
    ['components/abstract', 'pixi'],
    function (ComponentAbstractClass, PIXI) {
        var ComponentExpProgressBarClass = function () {
            ComponentAbstractClass.call(this);

            this.assetList = {
                base : {
                    url: 'img/black_pixel.png',
                    texture: null,
                    width : 454,
                    height : 49
                },
                empty : {
                    url: 'img/white_pixel.png',
                    texture: null,
                    width : 450,
                    height : 45
                },
                filled : {
                    url: 'img/blue_pixel.png',
                    texture: null
                }
            }
        };

        ComponentExpProgressBarClass.prototype = Object.create(ComponentAbstractClass.prototype);
        ComponentExpProgressBarClass.prototype.constructor = ComponentAbstractClass;

        /**
         * @lends ComponentDefaultProgressBarClass
         */
        ComponentExpProgressBarClass.prototype.extend({

            /**
             * Initialize function
             */
            init: function () {
                //Background
                this.base = new PIXI.Sprite(this.assetList.base.texture);
                this.container.addChild(this.base);

                this.base.width = this.assetList.base.width;
                this.base.height = this.assetList.base.height;

                //Empty part
                this.empty = new PIXI.Sprite(this.assetList.empty.texture);
                this.container.addChild(this.empty);

                this.empty.width = this.assetList.empty.width;
                this.empty.height = this.assetList.empty.height;

                this.setCenterPosition.call(this.empty);

                //Filled part
                this.filled = new PIXI.Sprite(this.assetList.filled.texture);
                this.container.addChild(this.filled);

                this.filled.width = 0;
                this.filled.height = this.assetList.filled.height || this.assetList.empty.height;

                this.setCenterPosition.call(this.filled);
                this.filled.x = this.empty.x;

                //Text
                this.text = new PIXI.Text('', {
                    font : '20px Arial',
                    fill : 'white',
                    stroke : 'black',
                    strokeThickness : 4
                });
                this.container.addChild(this.text);

                //Container
                this.setCenterPosition();
                this.y = 20;
            },

            /**
             * Render method
             */
            onBeforeRender: function() {
                this.setCenterPosition.call(this.text);

                this.filled.width = this.getExpInfo().percent * this.empty.width;
                this.text.setText(this.getExpInfo().text);
            },

            /**
             * Returns player info for exp progress bar
             * @returns {{percent: Number, text: String}}
             */
            getExpInfo: function () {

                var result = {
                    percent: Math.floor(App.user.experience / App.user.level.experience * 100) / 100,
                    text: 'Exp: ' + App.user.experience + '/' + App.user.level.experience
                };

                return result
            }
        });

        return ComponentExpProgressBarClass;
    }
)