define(
    'components/abstract',
    ['pixi', 'async', 'EventEmitter'],
    function (PIXI, async, EventEmitter) {

        /**
         * Abstract class for Component
         *
         * @name ComponentAbstractClass
         * @namespace
         * @constructor
         */
        var ComponentAbstractClass = function (scene) {

            /**
             * Images list to load
             *
             * @type {Object}
             */
            this.assetList = {};

            /**
             * @type {DisplayObjectContainer}
             */
            this.container = new PIXI.DisplayObjectContainer();
            this.container.component = this;

            /**
             * @type {SceneAbstractClass}
             */
            this.scene = scene;

            this.container.interactive = true;

            this.container.click = this.container.tap = function (mouseData) {
                this.trigger('tap', mouseData);
            }.bind(this);

            this.container.touchstart = this.container.mousedown = function (mouseData) {
                this.trigger('touchstart', mouseData);
            }.bind(this);

            this.container.touchend = this.container.mouseup = function (mouseData) {
                this.trigger('touchend', mouseData);
            }.bind(this);

            this.x = this.y = 0;

            /**
             * Z-index of component
             *
             * @type {number}
             */
            this.zIndex = 0;

            EventEmitter.call(this);
        };

        ComponentAbstractClass.prototype = Object.create(EventEmitter.prototype);
        ComponentAbstractClass.prototype.constructor = EventEmitter;


        /**
         * @lends ComponentAbstractClass
         */
        ComponentAbstractClass.prototype.extend({

            /**
             * Initialization function
             *
             * @param {function} callback
             *
             */
            init: function (callback) {
                callback = callback || function () {};
                callback();
            },


            /**
             * Load asset list
             *
             * @param {function} callback
             */
            loadAssets: function (callback) {
                callback = callback || function () {};
                async.forEach(Object.keys(this.assetList), function (assetName, next) {
                    var asset = this.assetList[assetName];
                    this.loadAsset(asset, next);
                }.bind(this), callback);
            },


            /**
             * Load one asset
             *
             * @param {{url: String, texture: *}} asset
             * @param {Function} callback
             */
            loadAsset: function (asset, callback) {
                var loader = new PIXI.ImageLoader(asset.url, true);
                loader.addEventListener('loaded', function (scope) {
                    asset.texture = scope.content.texture;
                    callback();
                });
                loader.load();
            },

            /**
             * This method will be applied before render
             */
            onBeforeRender: function () {},

            /**
             * Sets center horizontal and vertical position
             *
             * @param {Object} parent
             */
            setCenterPosition: function (parent) {
                var containerWidth;
                var containerHeight;

                if (parent && parent.width && parent.height) {
                    containerWidth = parent.width;
                    containerHeight = parent.height;
                } else if (this.parent) {
                    containerWidth = this.parent.width;
                    containerHeight = this.parent.height;
                } else if (this.container && this.container.parent) {
                    containerWidth = this.container.parent.width;
                    containerHeight = this.container.parent.height;
                } else {
                    containerWidth = window.innerWidth;
                    containerHeight = window.innerHeight;
                }


                this.x = Math.floor((containerWidth - this.width) / 2);
                this.y = Math.floor((containerHeight - this.height) / 2);
            },

            /**
             * Set percentage position over parent
             *
             * @param {Number} x
             * @param {Number} y
             * @param {Object} parent
             */
            setPercentagePosition: function (x, y, parent) {

                var containerWidth;
                var containerHeight;

                if (parent && parent.width && parent.height) {
                    containerWidth = parent.width;
                    containerHeight = parent.height;
                } else if (this.parent) {
                    containerWidth = this.parent.width;
                    containerHeight = this.parent.height;
                } else if (this.container && this.container.parent) {
                    containerWidth = this.container.parent.width;
                    containerHeight = this.container.parent.height;
                } else {
                    containerWidth = window.innerWidth;
                    containerHeight = window.innerHeight;
                }

                this.x = (typeof x === 'number') ? Math.floor(containerWidth * x) : this.x;
                this.y = (typeof y === 'number') ? Math.floor(containerHeight * y) : this.y;
            },

            /**
             * Set X coord
             *
             * @param {Number} x
             */
            set x(x) {
                this.container.position.x = x;
            },

            /**
             * Set Y coord
             *
             * @param {Number} y
             */
            set y(y) {
                this.container.position.y = y;
            },

            /**
             * Returns X coord
             * @returns {Number}
             */
            get x() {
                return this.container.position.x;
            },

            /**
             * Returns Y coord
             * @returns {Number}
             */
            get y() {
                return this.container.position.y;
            },

            /**
             * Returns component width
             * @param {Number} w
             */
            set width(w) {
                this.container.width = w;
            },

            /**
             * Returns component height
             * @param {Number} h
             */
            set height(h) {
                this.container.height = h;
            },

            /**
             * Returns component width
             * @returns {Number}
             */
            get width() {
                return this.container.width;
            },

            /**
             * Returns component height
             * @returns {Number}
             */
            get height() {
                return this.container.height
            }

        });


        return ComponentAbstractClass;
    }
);