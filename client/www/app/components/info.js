define(
    'components/info',
    ['components/abstract', 'pixi', 'async'],
    function (ComponentAbstractClass, PIXI, async) {

        /**
         * Component box
         *
         * @name ComponentInfoClass
         * @namespace
         * @constructor
         * @param {SceneAbstractClass} scene
         */
        var ComponentInfoClass = function (scene) {
            ComponentAbstractClass.call(this, scene);

        };


        ComponentInfoClass.prototype = Object.create(ComponentAbstractClass.prototype);
        ComponentInfoClass.prototype.constructor = ComponentAbstractClass;


        /**
         * @lends ComponentInfoClass
         */
        ComponentInfoClass.prototype.extend({

            /**
             * Initialize function
             */
            init: function () {
                this.text = new PIXI.Text(this.getPlayerInfo(), {font:"20px Arial", fill:"black"});
                this.container.addChild(this.text);
            },

            /**
             * Render method
             */
            onBeforeRender: function () {
                this.text.setText(this.getPlayerInfo());
            },

            /**
             * Returns player info
             * @returns {string}
             */
            getPlayerInfo: function () {
                var playerInfoString = '';
                var playerInfoObj = {
                    //FPS: App.layout.renderedFPS,
                    Level: App.user.level.level,
                    Box: App.user.box.name + '(' + App.user.box.level + ')',
                    Money: App.user.money,
                    Friends: App.user.invitesUsed + '/' + App.user.invitesTotal,
                };



                var totalEffect = App.user.totalEffect;
                Object.keys(totalEffect).forEach(function(effectName) {
                    playerInfoObj['Effect (' + effectName + ')'] = totalEffect[effectName];
                });

                Object.keys(playerInfoObj).forEach(function (key) {
                    playerInfoString += key + ': ' + playerInfoObj[key] + '\n';
                });

                return playerInfoString;
            }




        });


        return ComponentInfoClass;
    }
);