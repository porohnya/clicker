/**
 * Created by Titus on 12.10.14.
 */
define(
    'scenes/shop',
    [
        'scenes/abstract',
        //Components
        'components/backButton',
        'components/shopItemList',
        'components/info'
    ],
    function (
        SceneAbstractClass,
        ComponentBackButtonClass,
        ComponentShopItemListClass,
        ComponentInfoClass
        ) {

        /**
         * Shop scene class
         *
         * @name SceneShopClass
         * @namespace
         * @constructor
         */
        var SceneShopClass = function () {
            SceneAbstractClass.call(this);

            this.components = {
                backButton: new ComponentBackButtonClass(this),
                itemList: new ComponentShopItemListClass(this),
                info: new ComponentInfoClass(this)
            };
        };

        SceneShopClass.prototype = Object.create(SceneAbstractClass.prototype);
        SceneShopClass.prototype.constructor = SceneAbstractClass;


        /**
         * @lends SceneShopClass
         */
        SceneShopClass.prototype.extend({


        });


        return SceneShopClass;
    }
);