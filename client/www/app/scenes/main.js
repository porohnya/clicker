define(
    'scenes/main',
    [
        'scenes/abstract',
        //Components
        'components/box',
        'components/info',
        'components/expProgressBar',
        'components/energyProgressBar',
        'components/drop',
        'components/loot',
        'components/shopButton'
    ],
    function (
        SceneAbstractClass,
        ComponentBoxClass,
        ComponentInfoClass,
        ComponentExpProgressBarClass,
        ComponentEnergyProgressBarClass,
        ComponentDropClass,
        ComponentLootClass,
        ComponentShopButtonClass
        ) {

        /**
         * Main scene class
         *
         * @name SceneMainClass
         * @namespace
         * @constructor
         */
        var SceneMainClass = function () {
            SceneAbstractClass.call(this);

            this.components = {
                box:                new ComponentBoxClass(this),
                info:               new ComponentInfoClass(this),
                expProgressBar :    new ComponentExpProgressBarClass(this),
                energyProgressBar:  new ComponentEnergyProgressBarClass(this),
                drop:               new ComponentDropClass(this),
                loot:               new ComponentLootClass(this),
                shopButton:         new ComponentShopButtonClass(this)
                
            };

        };

        SceneMainClass.prototype = Object.create(SceneAbstractClass.prototype);
        SceneMainClass.prototype.constructor = SceneAbstractClass;


        /**
         * @lends SceneMainClass
         */
        SceneMainClass.prototype.extend({


        });


        return SceneMainClass;
    }
);