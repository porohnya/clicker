define(
    'scenes/abstract',
    ['async','pixi'],
    function (async, PIXI) {

        /**
         * Abstract class for scene
         *
         * @name SceneAbstractClass
         * @namespace
         * @constructor
         */
        var SceneAbstractClass = function () {

            /**
             * @type {{}}
             */
            this.components = {};

            /**
             * @type {Stage}
             */
            this.stage = new PIXI.Stage('0x666666', true);


            this.hide();

        };


        /**
         * @lends SceneAbstractClass
         */
        SceneAbstractClass.prototype.extend({

            /**
             * Loads assets
             * @param {Function} callback
             */
            loadAssets: function (callback) {
                async.forEach(Object.keys(this.components), function (componentName, next) {
                    this.getComponent(componentName).loadAssets(next);
                }.bind(this), callback);
            },

            /**
             * init components
             */
            init: function () {
                this._loadComponents();
                this._attachComponents();
            },

            /**
             * Loads components
             *
             * @private
             */
            _loadComponents: function () {
                Object.keys(this.components).forEach(function (componentName) {
                    this.getComponent(componentName).init();
                }, this)
            },

            /**
             * Attach components to stage
             * @private
             */
            _attachComponents: function () {
                Object.keys(this.components).forEach(function (componentName) {
                    this.stage.addChild(this.components[componentName].container);
                }, this);

                this.stage.children.sort(function (a, b) {
                    return a.component.zIndex - b.component.zIndex;
                });
            },


            /**
             * Render update scene
             * @private
             */
            _renderUpdate: function () {
                Object.keys(this.components).forEach(function (componentName) {
                    this.components[componentName].onBeforeRender();
                },this);
            },


            /**
             * Return component by name
             *
             * @param {string} name
             */
            getComponent: function (name) {
                if (!this.components[name]) {
                    throw new Error('Component not found');
                }

                return this.components[name];
            },

            /**
             * Hides scene
             */
            hide: function () {
                this.stage.visible = false;
            },

            /**
             * Show scene
             */
            show: function () {
                this.stage.visible = true;
            },


            /**
             * Returns component bounds by
             * @param {String} componentName
             * @returns {Rectangle}
             */
            getComponentBounds: function (componentName) {
                if (!this.components[componentName]) {
                    throw new Error('Component not found');
                }

                return this.components[componentName].container.getBounds();
            }
        });


        return SceneAbstractClass;
    }
);