define(
    'modules/user',
    [
        'EventEmitter',
        'models/bagItem',
        'models/effect'
    ],
    function (
            EventEmitter,
            BagItemModel,
            EffectModel
        ) {

        var regenerationTime = 5 * 60;

        /**
         * User Class
         *
         * @namespace
         * @name UserClass
         * @param {Object} options
         * @constructor
         */
        var UserClass = function (options) {
            this.extend(options);
            EventEmitter.call(this);
        };

        UserClass.prototype = Object.create(EventEmitter.prototype);
        UserClass.prototype.constructor = EventEmitter;

        /**
         * @lends UserClass
         */
        UserClass.prototype.extend({

            /**
             * Ready flag
             * @type {Boolean}
             */
            isReady: false,

            /**
             * Unbox current box
             */
            unBox: function () {
                if (!this.isEnergyAvailable) {
                    return alert('No energy');
                }
                App.send('unBox', {});
            },

            /**
             * Update user
             * @param {Object} data
             */
            updateInfo: function (data) {
                var bagData = [];
                data.bag.forEach(function (element) {
                    bagData.push(new BagItemModel(element._params));
                });
                data.bag = bagData;
                var effectData = [];
                data.effects.forEach(function (element) {
                    effectData.push(new EffectModel(element._params));
                });
                data.effects = effectData;
                this.extend(data);
                this.emit('update');
            },


            /**
             * Buy item from shopModel
             *
             * @param {ShopItemModel} shopItem
             * @param {Number} count
             */
            buy: function (shopItem) {
                 App.send('buy', {
                     shopItemId: shopItem._id
                 });
            },

            /**
             * Use item from bag
             *
             * @param {Number} index
             */
            useBag: function (index) {
                App.send('useBag', {
                    index: index
                });
            },

            /**
             * Returns current box
             * @returns {BoxModel}
             */
            get box() {
                return App.dictionary.getModel('Box', this.boxId);
            },

            /**
             * Returns current level
             * @returns {LevelModel}
             */
            get level() {
                return App.dictionary.getModel('Level', this.levelId);
            },

            /**
             * Returns is energy available flag
             *
             * @returns {boolean}
             */
            get isEnergyAvailable() {
                return (this.level.energy + this.energyExtended - this.energySpent) > 0;
            },


            /**
             * Returns available energy count
             * @returns {Number}
             */
            get energyAvailable() {
                return this.level.energy - this.energySpent + this.energyExtended;
            },

            /**
             * Returns number of seconds to restore energy
             *
             * @returns {number}
             */
            get secondsToRestoreEnergy() {
                if (this.startEnergyRegenerationDate === 0) {
                    return 0;
                }

                var seconds = this.startEnergyRegenerationDate + regenerationTime - App.nowTS();

                if (seconds > regenerationTime || seconds < 0) {
                    seconds = 0;
                }

                return seconds;
            },

            /**
             * Returns beautified object for timer to restore energy
             *
             * @returns {{minutes: string, seconds: string}}
             */
            get timerToRestoreEnergy() {
                var minutes = 0;
                var seconds = 0;

                if (this.secondsToRestoreEnergy > 0) {
                    minutes = Math.floor(this.secondsToRestoreEnergy / 60);
                    seconds = this.secondsToRestoreEnergy - minutes * 60;
                }

                var minutesString = (minutes < 10) ? '0' + minutes.toString() : minutes.toString();
                var secondsString = (seconds < 10) ? '0' + seconds.toString() : seconds.toString();

                return {
                    minutes: minutesString,
                    seconds: secondsString
                }
            },

            /**
             * Returns total effect
             * @returns {{saveProgress: number, fixLevel: number}}
             */
            get totalEffect() {
                var totalEffect = {
                    saveProgress: 0,
                    fixLevel: 0
                };

                var keys = Object.keys(totalEffect);

                this.effects.forEach(function (effect) {
                    keys.forEach(function (effectName) {
                        if (effect[effectName]) {
                            totalEffect[effectName] += effect[effectName];
                        }
                    });
                });

                return totalEffect;
            },

            /**
             * Collects drop
             * @param {Number} index
             */
            collectDrop: function (index) {
                App.send('collectDrop', {index: index});
            }
        });

        return UserClass;
    }
);