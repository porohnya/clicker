define(
    'modules/controller',
    [],
    function () {

        /**
         * @name ControllerClass
         * @namespace
         * @constructor
         */
        var ControllerClass = function () {
        };

        /**
         * Default Action
         * @param {Object} data
         */
        ControllerClass.prototype.defaultAction = function (data) {
            console.log('Default action', data);
        };


        /**
         * Error Action
         * @param {Object} data
         */
        ControllerClass.prototype.errorAction = function (data) {
            console.error('Error action: ', data);
        };

        /**
         * Who are you Action
         * @param {Object} data
         */
        ControllerClass.prototype.whoareyouAction = function (data) {
            var devHash = App.getHashParams().uuid;
            this.send('auth', {uuid: (devHash) ? devHash : window.device.uuid});
        };

//        /**
//         * Authorization Action
//         * @param {Object} data
//         */
//        ControllerClass.prototype.authorizedAction = function (data) {
//            if (!data.success) {
//                App.transport.socket.disconnect();
//                return alert(data.msg);
//            }
//            this.send('getPlayerInfo', {});
//
//        };

        /**
         * Authorization Action
         * @param {Object} data
         */
        ControllerClass.prototype.authorizedAction = function (data) {
            if (!data.success) {
                App.transport.socket.disconnect();
                console.error(data.msg);
                setTimeout(function () {
                    location.reload();
                },500);
                return
            }
            this.send('getPlayerInfo', {});

        };

        /**
         * Info Action
         * @param {Object} data
         */
        ControllerClass.prototype.playerInfoAction = function (data) {
            App.user.updateInfo(data);
            App.user.isReady = true;
            App.user.emit('ready');

            setTimeout(function () {
                App.user.emit('update');
            }, 0);

        };

        /**
         * Update user data
         * @param {Object} data
         */
        ControllerClass.prototype.userUpdateAction = function (data) {
            if (!App.user.isReady) {
                return;
            }


            App.user.updateInfo(data.user);
            if (App.user.unallocatedDrop.length) {
                //App.layout.showUnallocatedDrop(App.user.unallocatedDrop);
                //this.layout.box.setInteractive(false);
            } else {
                //this.layout.box.setInteractive(true);
            }

        };

        /**
         * Unbox event
         * @param {Object} data
         */
        ControllerClass.prototype.unBoxResultAction = function (data) {
            App.user.boxId = data.boxId;
            data.drops.forEach(function (dropResult) {
                console.log(dropResult);
                switch (true) {
                    case dropResult.money > 0:
                        App.layout.getScene('main').components.drop.animate('money',dropResult.money);
                        break;
                    case dropResult.experience > 0:
                        App.layout.getScene('main').components.drop.animate('experience',dropResult.experience);
                        break;
                }
            }, this);
        };

        /**
         * Dictionary info
         * @param {Object} data
         */
        ControllerClass.prototype.dictionaryAction = function (data) {
            App.dictionary.fill(data.models);
        };


        return ControllerClass;
    }
);