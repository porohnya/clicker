define(
    'modules/layout',
    [
        //Etc
        'pixi',
        'async',
        //Scenes
        //'scenes/preloader',
        'scenes/main',
        'scenes/shop'
    ],
    function (
            PIXI, async,
            SceneMain,
            SceneShop
        ) {

        var LayoutClass = function () {

            /**
             * Scene classes list
             * @type {Object}
             * @private
             */

            this._sceneInstances = {
                main: new SceneMain(),
                shop: new SceneShop()
            };


            /**
             * @type {SceneAbstractClass}
             * @private
             */
            this._currentScene = this.getScene('main');

            /**
             * @type {autoDetectRenderer}
             * @private
             */
            this._renderer = new PIXI.CanvasRenderer(window.innerWidth, window.innerHeight, null, true);

            document.body.appendChild(this._renderer.view);


        };

        /**
         * @lends LayoutClass
         */
        LayoutClass.prototype.extend({

            /**
             * Assets pre-loading
             * @param {function} callback
             */
            loadAssets: function (callback) {
                async.forEach(Object.keys(this._sceneInstances), function (sceneInstanceName, next) {
                    this.getScene(sceneInstanceName).loadAssets(next);
                }.bind(this), callback);
            },

            /**
             * Initialize all scenes
             */
            init: function () {
                Object.keys(this._sceneInstances).forEach(function (sceneInstanceName) {
                    this.getScene(sceneInstanceName).init();
                }, this);
                this.showScene('main');
                this.animate();
            },


            /**
             * Returns scene instance by name
             * @param {String} name
             * @returns {*}
             */
            getScene: function (name) {
                if (!this._sceneInstances[name]) {
                    throw new Error('Unknown scene');
                }

                return this._sceneInstances[name];
            },


            /**
             * Shows scene by name
             * @param {String} name
             */
            showScene: function (name) {
                this._currentScene.hide();
                this._currentScene = this.getScene(name);
                this._currentScene.show();
            },

            /**
             * Animates current scene stage
             */
            animate: function () {
                this._currentScene._renderUpdate();
                this._renderer.render(this._currentScene.stage);
                requestAnimationFrame(this.animate.bind(this));
            }
        });


        return LayoutClass;
    }
);