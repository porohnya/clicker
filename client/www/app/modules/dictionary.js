define(
    'modules/dictionary',
    ['EventEmitter'],
    function (EventEmitter) {

        /**
         * @name DictionaryClass
         * @namespace
         * @constructor
         */
        var DictionaryClass = function () {
            this._list = {};

            EventEmitter.call(this);
        };


        DictionaryClass.prototype = Object.create(EventEmitter.prototype);
        DictionaryClass.prototype.constructor = EventEmitter;

        /**
         * @lends DictionaryClass
         */
        DictionaryClass.prototype.extend({

            /**
             * Fill dictionary models
             * @param {Object} modelsData
             */
            fill: function (modelsData) {
                Object.keys(modelsData).forEach(function (modelName) {
                    Object.keys(modelsData[modelName]).forEach(function (modelId) {
                        if (!App.modelClasses[modelName]) {
                            return;
                        }
                        var modelInstance = new App.modelClasses[modelName](modelsData[modelName][modelId]);
                        this.addModel(modelName, modelInstance);
                    }, this);
                }, this);

                this.emit('ready');
            },


            /**
             * Add model to storage
             *
             * @param {String} modelName
             * @param {ModelClass} modelInstance
             */
            addModel: function (modelName, modelInstance) {
                if (!this._list[modelName]) {
                    this._list[modelName] = {};
                }

                this._list[modelName][modelInstance._id] = modelInstance;
            },


            /**
             * Returns model by id
             *
             * @param {String} modelName
             * @param {Number} modelId
             * @returns {ModelClass}
             */
            getModel: function (modelName, modelId) {
                return this._list[modelName][modelId];
            },


            /**
             * Returns full model list
             * @param {String} modelName
             * @returns {Object}
             */
            getModelList: function (modelName) {
                return this._list[modelName];
            }

        });

        return DictionaryClass;
    }
);