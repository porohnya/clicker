define(
    'modules/transport',
    ['socketio', 'async'],
    function (io) {

        var TransportClass = function (url, dispatcher) {
            /**
             * Url to connect
             * @type {*}
             */
            this.url = url;

            /**
             * Dispatcher
             * @type {Function}
             */
            this.dispatcher = dispatcher;
        };


        /**
         * Connects via socket.io
         * @param {String} url
         * @param {Function} callback
         */
        TransportClass.prototype.connect = function (callback) {
            this.socket = io.connect(this.url)
            this.socket.on('connect', function () {
                console.log('Connection established');
                callback();
            });

            this.socket.on('error', function (error) {
                callback('Connection error');
            });


            this.socket.on('disconnect', function () {
                console.log('Connection lost');
            });


            this.socket.on('message', this.onMessage.bind(this));
        };


        /**
         * Message interface
         * @param message
         */
        TransportClass.prototype.onMessage = function (message) {
            this.dispatcher(message);
        };

        /**
         * Send message to server
         * @param {String} action
         * @param {Object} params
         */
        TransportClass.prototype.send = function (action, params) {
            this.socket.emit('message', {action: action, data: params});
        };


        return TransportClass;
    }
);