/**
 * Fake Device API if running non-device
 */
(function(){
    if (document.URL.indexOf("http://") === -1) {
        return;
    }

    window.device = {
        model: 'LG-P765',
        cordova: '2.9.0 JS=2.9.0-0-g83dc4bd',
        platform: 'Android',
        uuid: 'a3dd7c841a1f9609',
        available: true,
        version: '4.1.2'
    }
}());

